/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.tools.idea.mlkit.lightpsi;

class ClassNames {
  public static String BITMAP = "android.graphics.Bitmap";
  public static String BYTE_BUFFER = "java.nio.ByteBuffer";
  public static String CONTEXT = "android.content.Context";
  public static String IO_EXCEPTION = "java.io.IOException";
  public static String TENSOR_IMAGE = "org.tensorflow.lite.support.image.TensorImage";
  public static String TENSOR_BUFFER = "org.tensorflow.lite.support.tensorbuffer.TensorBuffer";
  public static String TENSOR_LABEL = "org.tensorflow.lite.support.label.TensorLabel";
}
